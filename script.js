var swbEditor = function (options,ele) {
    var $buttongroup = "<div class='vline'></div>";
    var $button = "<button></button>";

    var showSourcecode = false;
    var name = "";
    var vars = {
        undo  : true,
        redo  : true,
        fontname  : true,
        fonts  : [],
        style  : true,
        styles  : [],
        bold  : true,
        italic  : true,
        left  : true,
        center  : true,
        right  : true,
        justify  : true,
        ul  : true,
        ol  : true,
        indent  : true,
        outdent  : true,
        hr  : true,
        link  : true,
        unlink  : true,
        color  : true,
        image  : true,
        imagecall  : "",
        code  : true,
        placeholder  : true,
        placeholderlist  : [],
    };

    this.constructor = function (options,ele) {
        $.extends(vars,options,ele);
        name = options.name;
        Output();
    };

    var Output = function () {
        var commands = $('<div></div>').attr({
            class:"swb_editor_commands"
        });
        var frame = $('<iframe></iframe>').attr({
            name:options.name
        });
        var history = $($buttongroup),
            font = $($buttongroup),
            format = $($buttongroup),
            style = $($buttongroup),
            align = $($buttongroup),
            list = $($buttongroup),
            view = $($buttongroup),
            link = $($buttongroup),
            other = $($buttongroup);

        if (vars.undo) {
            history.append($($button).html('<i class="fas fa-undo"></i>').onclick(execCmd('undo')));
        }
        if (vars.redo) {
            history.append($($button).html('<i class="fas fa-redo"></i>').onclick(execCmd('redo')));
        }
        if (vars.fontname) {
            var option = "";
            vars.fonts.forEach(
              option += "<option></option>"
            );
            font.append($("<select></select>").html(option).onchange(execCmdWithArgs('fontName',this.value)));
        }
        if (vars.style) {
            var option = "";
            vars.styles.forEach(
                option += "<option></option>"
            );
            format.append($("<select></select>").html(option).onchange(execCmdWithArgs('formatBlock',this.value)));
        }
        if (vars.bold) {
            style.append($($button).html('<i class="fas fa-bold"></i>').onclick(execCmd('bold')));
        }
        if (vars.italic) {
            style.append($($button).html('<i class="fas fa-italic"></i>').onclick(execCmd('italic')));
        }
        if (vars.left) {
            align.append($($button).html('<i class="fas fa-align-left"></i>').onclick(execCmd('justifyLeft')));
        }
        if (vars.center) {
            align.append($($button).html('<i class="fas fa-align-center"></i>').onclick(execCmd('justifyCenter')));
        }
        if (vars.right) {
            align.append($($button).html('<i class="fas fa-align-right"></i>').onclick(execCmd('justifyRight')));
        }
        if (vars.justify) {
            align.append($($button).html('<i class="fas fa-align-justify"></i>').onclick(execCmd('justifyFull')));
        }
        if (vars.ul) {
            list.append($($button).html('<i class="fas fa-list-ul"></i>').onclick(execCmd('InsertUnorderedList')));
        }
        if (vars.ul) {
            list.append($($button).html('<i class="fas fa-list-ol"></i>').onclick(execCmd('InsertOrderedList')));
        }
        if (vars.indent) {
            view.append($($button).html('<i class="fas fa-indent"></i>').onclick(execCmd('indent')));
        }
        if (vars.outdent) {
            view.append($($button).html('<i class="fas fa-outdent"></i>').onclick(execCmd('outdent')));
        }
        if (vars.hr) {
            view.append($($button).html('<i class="fas fa-minus"></i>').onclick(execCmd('insertHorizontalRule')));
        }
        if (vars.link) {
            link.append($($button).html('<i class="fas fa-link"></i>').onclick(execCmdWithArgs('createlink',prompt('Enter a URL','http://'))));
        }
        if (vars.unlink) {
            link.append($($button).html('<i class="fas fa-unlink"></i>').onclick(execCmd('unlink')));
        }
        if (vars.color) {
            other.append($("<input>").attr({type:"color"}).onchange(execCmdWithArgs('foreColor',this.value)));
        }
        if (vars.image) {
            other.append($($button).html('<i class="fas fa-image"></i>').onclick(execCmdWithArgs('insertImage',prompt('Enter the image URL',''))));
        }
        if (vars.code) {
            other.append($($button).html('<i class="fas fa-code"></i>').onclick(toggleSource()));
        }
        if (vars.placeholder) {
            other.append($($button).text('[P]').onclick(execCmd('bold')));
        }
    };
    this.enableEditMode = function () {
        name.document.designMode = "On";
    };
    this.execCmd = function (command) {
        name.document.execCommand(command,false,null);
    };
    this.execCmdWithArgs = function (command,args) {
        name.document.execCommand(command,false,args);
    };
    this.toggleSource = function () {
        if (showSourcecode) {
            name.document.getElementsByTagName('body')[0].innerHTML = name.document.getElementsByTagName('body')[0].textContent;
            showSourcecode = false;
        } else {
            name.document.getElementsByTagName('body')[0].textContent = name.document.getElementsByTagName('body')[0].innerHTML;
            showSourcecode = true;
        }
    };
}